FROM python:3.7-alpine

RUN apk add --update gcc \
    musl-dev \
    postgresql-dev

WORKDIR /app
COPY requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

COPY . /app

EXPOSE 8000 8000

ENTRYPOINT ["python", "-m", "app"]