/* use raw SQL instead of alembic for migrations
 just to decrease project setup time */

CREATE TABLE repositories (
  id SERIAL PRIMARY KEY,
  full_name VARCHAR(256) NOT NULL,
  html_url VARCHAR(256) NOT NULL,
  description VARCHAR,
  stars_count NUMERIC NOT NULL,
  language VARCHAR(64)
);