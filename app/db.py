import logging

from aiohttp import web
from aiopg.sa import create_engine


async def connect_db(app: web.Application):
    app.db = await create_engine(
        app.db_uri
    )
    logging.info('Setup db connection')


async def disconnect_db(app: web.Application):
    if app.db:
        app.db.close()
        await app.db.wait_closed()
        logging.info('Closed db connection')
