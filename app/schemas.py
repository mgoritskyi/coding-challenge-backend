from marshmallow import Schema, fields


class RepositorySchema(Schema):
    full_name = fields.String()
    html_url = fields.String()
    description = fields.String()
    stars_count = fields.Integer()
    language = fields.String()
