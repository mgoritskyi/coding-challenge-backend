import sqlalchemy as sa


metadata = sa.MetaData()


repositories_table = sa.Table(
    'repositories',
    metadata,
    sa.Column(
        'id',
        sa.INTEGER(),
        primary_key=True,
    ),
    sa.Column(
        'full_name',
        sa.VARCHAR(),
        nullable=False,
    ),
    sa.Column(
        'html_url',
        sa.VARCHAR(),
        nullable=False,
    ),
    sa.Column(
        'description',
        sa.VARCHAR(),
        nullable=True,
    ),
    sa.Column(
        'stars_count',
        sa.NUMERIC(),
        nullable=False,
    ),
    sa.Column(
        'language',
        sa.VARCHAR(),
        nullable=True,
    ),
)
