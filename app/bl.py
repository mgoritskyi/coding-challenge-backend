import math
import logging

from aiohttp.client import ClientSession
from aiopg.sa import Engine

from app.tables import repositories_table


GITHUB_URL = 'https://api.github.com/search/repositories?' \
             'q=language:python+stars:>500&per_page={}&page={}'
RESULTS_PER_PAGE = 1000
ASC = 'asc'
DESC = 'desc'

log = logging.getLogger(__name__)


async def load_repositories_list() -> list:
    """Retrieve Github repositories list"""
    repositories_list = []
    async with ClientSession() as session:
        # fetch first page to get total count
        response = await session.get(
            GITHUB_URL.format(RESULTS_PER_PAGE, 1)
        )
        response = await response.json()
        total_count = math.ceil(
            response['total_count'] / RESULTS_PER_PAGE
        )
        repositories_list.extend(response['items'])

        # iterate over rest pages
        for page in range(2, total_count + 1):
            response = await session.get(
                GITHUB_URL.format(RESULTS_PER_PAGE, page)
            )
            response = await response.json()
            if response and response.get('items'):
                repositories_list.extend(response['items'])
            else:
                log.error(response)

    return repositories_list


async def download_repositories(db: Engine):
    """Copy Github repositories list to database"""
    raw_list = await load_repositories_list()

    prepared_list = [
        {
            'full_name': repo['full_name'],
            'language': repo['language'],
            'html_url': repo['html_url'],
            'description': repo['description'],
            'stars_count': repo['stargazers_count'],

        }
        for repo in raw_list
    ]

    async with db.acquire() as connection:
        await connection.execute(
            repositories_table.insert().values(prepared_list)
        )


async def get_repositories(
    db: Engine,
    page: int,
    per_page: int,
    sorting: str = None,
):
    """Get repositories list from db"""
    query = repositories_table.select().offset(
        page * per_page
    ).limit(per_page)

    if sorting == ASC:
        query = query.order_by(repositories_table.c.stars_count.asc())
    elif sorting == DESC:
        query = query.order_by(repositories_table.c.stars_count.desc())

    async with db.acquire() as connection:
        raw_result = await connection.execute(query)
        result = await raw_result.fetchall()

    return result
