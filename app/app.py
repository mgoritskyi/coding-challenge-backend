import logging
import os

from aiohttp import web
from aiojobs.aiohttp import setup

from app.db import connect_db, disconnect_db
from app.views import get_repositories_list, download_list


def run_app():
    app = web.Application()

    db_uri = os.environ.get('DB_URI')
    if not db_uri:
        logging.error('No database uri found')
        return

    app.db_uri = db_uri

    app.add_routes([
        web.get('/repositories', get_repositories_list),
        web.post('/repositories/download', download_list),
    ])

    app.on_startup.append(connect_db)
    app.on_cleanup.append(disconnect_db)

    setup(app)

    web.run_app(
        app,
        host='0.0.0.0',
        port=8000,
    )
