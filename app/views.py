from aiohttp.web import json_response
from aiojobs.aiohttp import spawn

from app.bl import download_repositories, get_repositories
from app.schemas import RepositorySchema

RESULTS_PER_PAGE = 50


async def get_repositories_list(request):
    page = int(request.query.get('page', 1))
    per_page = int(request.query.get('per_page', RESULTS_PER_PAGE))
    order = request.query.get('order')
    resp = await get_repositories(
        request.app.db,
        page,
        per_page,
        order,
    )
    return json_response(
        RepositorySchema().dump(resp, many=True).data  # serialize data
    )


async def download_list(request):
    # spawn background task to upload github repos list
    await spawn(
        request,
        download_repositories(request.app.db)
    )
    return json_response({'status': 'ok'})
