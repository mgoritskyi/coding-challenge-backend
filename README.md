#Solution

## How to run
````
$ docker-compose up -d database & docker-compose up app
````
## Usage
### Get repositories list
````
curl -X GET http://localhost:8000/repositories?page=1&per_page=20&order=asc
````
### Initialize repositories download
````
curl -X POST http://localhost:8000/repositories/download
````
## Implementation description
To implement API server was used async
web-framework aiohttp. Aiojobs was used
to run repositories download in background.
As a storage I used PostgreSQL and to perform
requests - SQLAlchemy+aiopg. Everything is
running in Docker containers.
